
setClass(
  Class = "form",
  representation = representation(title = "character", description = "character", form = "list"),
  validity = function(object){
    if(!RJSONIO:::isValidJSON(toJSON(object@form), TRUE)) {
      stop("The argument is not a valid JSON list")
    } 
    if(length(object@form$schema$properties) != length(object@form$options$fields)) {
      stop("The number of element schemas and options must match")
    }
    return(TRUE)
  }
)

#' form class initializor
#' 
#' Method that runs when a new form object is initialized.
setMethod (
  f = "initialize",
  signature = "form",
  definition = function(.Object, title = NULL, description = NULL, form = list()) {
    
    .Object@form <- form
    
    # Schema
    .Object@form$schema$title <- title
    .Object@form$schema$description <- description
    .Object@form$schema$type <- "object"

    # Options
    .Object@form$options$renderForm <- TRUE
    .Object@form$options$form$attributes$method <- "post"
    .Object@form$options$form$buttons$submit <- list()
    .Object@form$options$form$buttons$reset <- list()
    
    return(.Object)
  }
)

#' Create new form
#' 
#' Use this to create a new form object.
#' @export
#' @param title title
#' @param description description
#' @param form list of form schema and options
#' @examples
#' form <- form()
form <- function(...) {
  new(Class = "form", ...)
}

#' Get list of form object
#' 
#' Method to get the list of a form object.
#' @export
#' @docType methods
#' @rdname get_form
#' @examples
#' get_form(form)
setGeneric("get_form", function(object){ standardGeneric("get_form") })

#' @rdname get_form
setMethod(
  f = "get_form", 
  signature = "form",
  definition = function(object){
    return(object@form)
  }
)

#' Set list of form object
#' 
#' Method to set the list of a form object. 
#' It is recommended to use add_field() instead,
#' but this could be used when using customized forms.
#' @export
#' @docType methods
#' @rdname set_form
#' @examples
#' set_form(form) <- list()
setGeneric("set_form<-", function(object, value){ standardGeneric("set_form<-") })

#' @rdname set_form
setMethod(
  f = "set_form<-",
  signature = "form",
  definition = function(object, value){
    object@form <- value
    validObject(object)
    return(object)
  }
)

#' Add form field
#' 
#' Method to add field to form object. Newly added fields gets an accumulated ID starting from 1. Each field must have a schema as well as an options list.
#' @param object form object
#' @param value list including schema and options
#' @export
#' @examples
#' add_field(form) <- list(schema = list(), options = list())
#' @docType methods
#' @rdname add_field
setGeneric("add_field<-", function(object, value){ standardGeneric("add_field<-") })

#' @rdname add_field
setMethod(
  f = "add_field<-",
  signature = "form",
  definition = function(object, value){
    if(is.null(value$schema)) value$schema <- list()
    if(is.null(value$options)) value$options <- list()

    object@form$schema$properties[[length(object@form$schema$properties) + 1]] <- value$schema
    object@form$options$fields[[length(object@form$options$fields) + 1]] <- value$options
    validObject(object)
    return(object)
    }
)

#' Show method
#'
#' Uses the ordinary show() method, and when used it shows the number of form fields.
#' @export
#' @examples
#' show(form)
setMethod(
  f = "show",
  signature = "form",
  definition = function(object){
    cat(sprintf("form contains %s elements \n", length(object@form$schema$properties)))
  })

#' Convert form to JSON
#'
#' Method to convert a form object to a JSON character string.
#' @param form object
#' @export
#' @docType methods
#' @rdname to_json
setGeneric("to_json", function(object){ standardGeneric("to_json") })

#' @rdname to_json
setMethod(
  f = "to_json",
  signature = "form",
  definition = function(object){
    json <- RJSONIO:::toJSON(get_form(object))
    json <- gsub("\\[\\]", "\\{\\}", json) # Alpaca FIX - otherwise buttons will show text fields instead
    return(json)
  })

#' Print form as HTML
#'
#' Convert a form object to HTML.
#' After building a form one usually wants to convert it to a HTML character string.
#' The JavaScript files are included in the webforms package, and thus automatically added to the HTML file.
#' However, the CSS urls has to be added (at the moment they aren't automatically included due to relative paths to images).
#' @param x form object
#' @param css character vector of http paths to css files to use (default = NULL).
#' @export
setMethod(
  f = "print",
  signature = "form",
  definition = function(x, css = NULL){
    html.css <- if (!is.null(css)) {
      webtools:::files_to_html(webtools:::filter_files(css, c("css")))  
    } else ""   # HTML including CSS urls
    js <- webtools:::search_files(file.path(system.file(package="webforms"), "deps"), c("js"))  # JavaScript files read from package
    html.js <- webtools:::files_to_html(js)  # HTML including JavaScript code 
    html.js_func <- sprintf('<script type="text/javascript">$(function() { $("#alpaca").alpaca(%s);});</script><div id="alpaca"></div>', to_json(x))  # HTML including Alpaca form object
    html <- paste(html.css, html.js, html.js_func)  # Put it all together, observe that CSS files are loaded firstly
    return(html)
  }
)