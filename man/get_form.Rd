\docType{methods}
\name{get_form}
\alias{get_form}
\title{Get list of form object}
\description{
  Method to get the list of a form object.
}
\examples{
get_form(form)
}

