\name{form}
\alias{form}
\title{Create new form}
\usage{
  form(...)
}
\arguments{
  \item{title}{title}

  \item{description}{description}

  \item{form}{list of form schema and options}
}
\description{
  Use this to create a new form object.
}
\examples{
form <- form()
}

