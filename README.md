webforms: Easy HTML5 Forms in R
===============================

The webforms package use [Alpaca](https://github.com/gitana/alpaca) by Yong Qu and Michael Uzquiano to generate nice and simple HTML5 forms.

## Install

The package requires [webtools](https://github.com/reinholdsson/webtools) (also available on GitHub).
Neither of those packages are available at CRAN yet, but they can be installed by following the below instructions.

First, if necessary, install [devtools](https://github.com/hadley/devtools) from CRAN:
 
    install.packages("devtools")

Then install the web packages from GitHub:

    library("devtools")
    install_github("webtools", "reinholdsson")
    install_github("webforms", "reinholdsson")
	
And then you are ready to go by simply loading the package:

    library("webforms")

## How to use

Please take a look at the [Wiki](https://github.com/reinholdsson/webforms/wiki) pages.

## License

The webforms package is licensed under the [GPL-2](http://cran.r-project.org/web/licenses/GPL-2) license.